.model small
.code
org 100h
start:
   jmp mulai
nama   db 13,10,'Nama : $'
hp     db 13,10,'No HP : $'        
tampak_nama db 30,?,30 dup (?)
tampak_hp   db 13,?,13 dup (?)
tampak_kode db 13,?,13 dup (?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar db 13,10,'============================================='
       db 13,10,'      DAFTAR SEWA PLAYSTATION RENTAL.ID      '
       db 13,10,'============================================='
       db 13,10,'NO|     Tipe     |   Tahun |   Harga per jam|'
       db 13,10,'1.| PLAYSTATION 1|   2021  |   Rp. 1.000/jam|'
       db 13,10,'2.| PLAYSTATION 2|   2021  |   Rp. 3.000/jam|'
       db 13,10,'3.| PLAYSTATION 3|   2021  |   Rp. 6.000/jam|'
       db 13,10,'4.| PLAYSTATION 4|   2021  |   Rp. 7.000/jam|'
       db 13,10,'5.| PLAYSTATION 5|   2021  |   Rp. 9.000/jam|' 
       db 13,10,'============================================='
       db 13,10,'     TENTUKAN PLAYSTATION PILIHAN ANDA       '
       db 13,10,'=============================================$'
      
error   db 13,10,'ANDA SALAH MEMASUKKAN KODE $'
pilih_ps db 13,10,'Silahkan masukkan tipe playstation yang anda sewa $'
succes db 13,10,'SELAMAT $'

   mulai:
   mov ah,09h
   lea dx,nama
   int 21h
   mov ah,0ah
   lea dx,tampak_nama
   int 21h
   push dx
   
   mov ah,09h
   lea dx,hp
   int 21h
   mov ah,0ah
   lea dx,tampak_hp
   int 21h
   push dx
   
   mov ah,09h
   mov dx,offset daftar
   int 21h
   mov ah,09h
   mov ah,01h

   mov ah,09h 
   mov ah,01h
   jmp proses
   jne error_msg

error_msg:
   mov ah,09h
   mov dx,offset error
   int 21h
   int 20h

proses:
  mov ah,09h
  mov dx,offset pilih_ps
  int 21h
  
  mov ah,1
  int 21h
  mov bh,al
  mov ah,1
  int 21h
  mov bl,al
  cmp bh,'0'
  cmp bl,'1'
  je hasil1
  
  cmp bh,'0'
  cmp bl,'2'
  je hasil2
  
  cmp bh,'0'
  cmp bl,'3'
  je hasil3
  
  cmp bh,'0'
  cmp bl,'4'
  je hasil4
  
  cmp bh,'0'
  cmp bl,'5'
  je hasil5
  
  jne error_msg
  
;================================
hasil1:
  mov ah,09h
  lea dx,teks1
  int 21h
  int 20h
  
hasil2:
  mov ah,09h
  lea dx,teks2
  int 21h
  int 20h     
  
hasil3:
  mov ah,09h
  lea dx,teks3
  int 21h
  int 20h     
  
hasil4:
  mov ah,09h
  lea dx,teks4
  int 21h
  int 20h
  
hasil5:
  mov ah,09h
  lea dx,teks5
  int 21h
  int 20h
  
  
;================================='
teks1 db 13,10,'Kamu memilih PLAYSTATION 1'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 1.000'
      db 13,10,'Terima Kasih $'
      
teks2 db 13,10,'Kamu memilih PLAYSTATION 2'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 3.000'
      db 13,10,'Terima Kasih $'
      
teks3 db 13,10,'Kamu memilih PLAYSTATION 3'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 6.000'
      db 13,10,'Terima Kasih $'                           
      
teks4 db 13,10,'Kamu memilih PLAYSTATION 4'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 7.000'
      db 13,10,'Terima Kasih $'                             
      
teks5 db 13,10,'Kamu memilih PLAYSTATION 5'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 9.000'
      db 13,10,'Terima Kasih $'    
      
end start
